# Deskribapena

Aplikazioa zati ezberdinetan banatu dugu.
1. Hasierako ikuspegia.
2. Erabiltzaileen administrazioa.
3. Sesioaren erabilera.
4. Pelikula bakoitzaren barne informazioa.
5. Itxura

## 1. Index
Bista honetan datubaseko pelikulak azaltzen dira.


Erabiltzaileak pantailan zenbat pelikula azaltzea nahi duen aukera dezake, eta
behin guztiak ikusi ondoren, "gehiago" sakatu pelikula berrak kargatzeko.

Gainera, bilatze barran nahi duen pelikularen izenaren zatiren bat jarri dezake,
 eta "Bilatu" sakatzean sistemak koinzidentzia guztiekin lista bat osatuko du.

Azkenik, edozein pelikula sakatzean, pelikula propio horren informazioa lortzen
den esteka bat lortzen da.

## 2. Erabiltzaileak
Sisteman bi erabiltzaile mota bereiziko dira, sisteman
erregistratuak eta erregistratu gabeak.

Bi erabiltzaile mota hauen desberdintasun
nagusia beraien abantailena izango da. Biek izango dute filmen
inguruko informazioa bilatzeko aukera, baina erregistratu direnek
soilik izango dute film baten inguruan beraien ikuspuntua emateko
aukera.

Edozein erabiltzailek sisteman erregistratzeko aukera izango du,
oinarrizko formulario bat betez:

![alt text Sing up irudia](http://i.imgur.com/6yyIJpv.png)

Nabaria denez, erabiltzaile berriek ezingo dituzte aurretik
aukeratutako erabiltzaile *izen* eta *e-postak* beraientzat ere
aukeratu.

## 3. Sesioa

Erabiltzaile erregistratuak saioa hasteko oinarrizko *login* bat
bete beharko du zeinean bere *e-posta* helbidea edo
 *erabiltzaile-izena* eta pasahitza sartu beharko dituen.

 Hurrengoa izango da *login* honen itxura:

 ![alt text Log in irudia](http://i.imgur.com/Gxq3xxu.png)

 Behin saioa hastean oinarrizko sesioen kudeaketa bat egingo da
 zeinean erabiltzaileari saioa isteko zein sistema erabiltzeko
 aukera emango zaion.

## 4. Pelikula

Ikuspegi honetan erabiltzaileak aukeratutako pelikularen
informazioa agertuko da, honi buruz, bai jende adituak, bai
sisteman erregistratuko erabiltzaileek egindako iruzkinekin
batera.

![alt text Pelikularen ikuspegia](http://i.imgur.com/21INk76.png)

Hasiera batean 5 iruzkin agertuko dira gehienez, hurrengo
bostak ikusi nahi badira *geihago* botoia sakatu beharko
da.

## 5. CSS

Web orri osoan "antiquewhite"
![Area argi bat, zuri zahartua bezala](http://i.imgur.com/Y9UjCb1.png)
 kolore lehuna jarri dugu, irakurlearentzat
erosoagoa izan dadin.

Gainera, estekei estilo dotoreago bat eman zaie
![Fondoko kolorearekin hobeto doan marroi bat](http://i.imgur.com/79v6PNM.png).
```
.gehiago
{
	text-decoration: 	underline;
	color: 	brown;
}
```

Izenburu guztiei fondoarekin bat doan kolorea eta formatua jarri zaie
![Pelikulak azaltzen da, erdian.kolorea urdina da, baina urdin lehun bat](http://i.imgur.com/YFox6Tf.png)

```
h1
{
	font-family: 	Monospace, Serif;
	color: royalblue;
	text-align: center;
}
```


Pelikulak koadro batean eta zentratuta jarri dira.
![pelikulen lista bat azaltzen da, izen eta argazki](http://i.imgur.com/8Yvowz6.png)

Pelikulei buruzko esteketan berriz, marroia eta zuria aukeratu dira koloretzat.
![(500) Days of Summer pelikulari buruzko hiru komentario azaltzen dira](http://i.imgur.com/Mv5MwIm.png)
```
div.iruzkina {
    width: 80%;
    margin-left: 10%;
    margin-right: 10%;
    margin-top: 2em;
    border: solid black 1px;
}
.ir_goiburua {
    background-color: #993300;
}
.egilea {
    font-weight: bold;
    font-size: 1.2em;
    color: #FFFFFF;
}
.ir_gorputza {
    text-align: justify;
    text-indent: 2em;
    margin: 1em;
}
span {
    padding: 1em;
}

```

# Kodearen deskribapena

Kodea esplikatzeko, banatze sistema berbera aplikatu dugu:


## 1. Index

Pelikulak azaltzeko sql erabili da, eta ajax bidez eguneratu dugu orria:

```
$result = $sql->query("SELECT id, name, image FROM films WHERE name LIKE '%$kw%'
 ORDER BY name LIMIT $p,$q;");
```

 eta

```
function pelikulak_atzitu (id, val) {...}
```

Hau orria kargatzean egiten da baita, ez soilik estekak sakatzean.

```
<body onload="javascript:pelikulak_atzitu('pelist','');">
```

Ajax bidez kargatzen diren pelikulak esteka bat izango dira, honako formatuan:

```
<a href=pelikula.php?id=someid id="someid">
    \t - Name: somename
    <img src="imageurl" alt="altText" height="130" width="130"/>
</a><br/>
```

## 2.Erabiltzaileen administrazioa

### *XML* fitxategia

Erabiltzaileen *izen* , *e-posta* eta *pasahitzak* gordetzeko
*XML* fitxategi bat erabili da. Konsziente gera hau ez dela
batere segurua, baina *XML* teknologia erabiltzea beharrezkoa
zenez eta filmak jada datu base batean zeudenez, ez genuen
beste aukerarik izan.

Gure *XML* fitxategiak hurrengo *.dtd*-ak azaltzen duen formatua
izango du:

```
<!ELEMENT erabiltzaileak (erabiltzaile)*>
<!ATTLIST erabiltzaileak azkenid IDREF #REQUIRED>
<!ELEMENT erabiltzaile (izena, pasahitza, eposta)>
<!ATTLIST erabiltzaile id ID #REQUIRED>
<!ELEMENT izena (#PCDATA)>
<!ELEMENT pasahitza (#PCDATA)>
<!ELEMENT salt (#PCDATA)>
<!ELEMENT eposta (#PCDATA)>
```

Bertan jada ikus daiteke, *XML*-en publikotasunak duen segurtasun
faltari aurre egiteko proposatzen dugun konponbide bat, definitiboa
ez dena, ezta gutxiagorik ere.

Gure proposamena *pasahitzen* *hashing* eta *salting* bat egitea
izan da. Honetarako **SHA1** *hashing* algoritmoaz baliatu gara.
Hurrengo kodearen bidez gordetzen dugu erabiltzailearen pasahitza
gure *XML* fitxategian:

```
$berria->addChild('pasahitza',sha1($salt.$pasahitza));
```

Noski hau egin aurretik ausazko *salt*-a sortu dugu, hurrengo kodeaz:

```
$salt = uniqid(mt_rand(), true);
```

Aipatu ere, *XML* fitxategiekin lan egiteko *php* liburutegi
estandarreko  *SimpleXML* erabili dugula.

### Errore kontrola

Erabiltzaile berri bat sortzeko garaian, formulario bat bete
behar denez, eremu hauen zuzentasuna egiaztatzeko bi funtizo
erabili dira, bat *javascript*-ekoa bezeroaren aldean eta
bestea *php* bidezkoa zerbitzariaren aldean.

*Javascript*-eko funtzioarekin datu okerrak
ahalik eta azkarren baztertuak izango dira, baina erabiltzailea
zerbitzariarekin zuzeanean komunikatzen bada, gure *XML*
fitxategian datu korruptuak saihestuko ditugu *php*-ko
 funtzioarekin.

## 3. Sesioa

### Sesioa hasi

Sesioak kudeatzeko, *php*-ko *_SESSION* superglobala erabili da.

Erabiltzaileak, formulario baten bidez bere erabiltzaile *izena* edo
 *e-posta* eta *pasahitza* zehaztuko ditu eta datuak egokiak diren
 kasuan zerbitzarira bidaliko dira, zeinean *php* fitxategi batean
 aztertuko diren. Atal hontako errore kontrola aurreko atalekoaren oso antzeko izango da.

 Behin datuak zerbitzarian jasotzean, *SimpleXML* -ren bidez
 gure *XML* fitxategian zehaztutako *izen* edo *e-posta*-rekin
 erabiltzailerik dagoen egiaztatuko dugu. Baiezkoa den kasuan,
 erabiltzaile honen *salt*-a jaso eta aurreko atalean azaldu
 bezala pasahitzaren *hash*-a lortuko dugu. Lortutako *hash*-a
 eta gure *XML* fitxategian zegoena bat badatoz, sesioa hasiko dugu
 *_SESSION* superglobala zehaztuz:
```
$_SESSION['izena'] = $izena;
$_SESSION['id'] = $user_id;
```

Ondoren balio hauek atzitu nahi ditugun bakoitzean ```session_start();``` funtzioari deitu besterik ez dugu egin behar.

### Sesioa Itxi

Sesioa ixteko aurretik zehaztutako superglobalak berriro hasieratu
beharko ditugu. Behin sesioa ixtean hasierako orrira itzuliko da.

```
session_start();
//sesioko superglobalak ezabatu
unset($_SESSION["izena"]);
unset($_SESSION["id"]);
session_destroy();
//Hasierako orrira itzuli.
header("Location: index.php");
```

## 4. Pelikula

Atal honetan film-ei buruzko informazioa agertzen da, honi
buruzko iruzkinekin batera.

Orri honetara sartzeko film bat aukeratua izan beharko da
*index.php* - n eta honi *GET* bidez pasako zaio honen *id*-a.

Behin *id*-a dugula *sql*-ko query baten bidez, *id* hori
duen filmareen informazioa lortuko da:

```
$result = $sql->query("SELECT * FROM films WHERE
 id=$pelikulaid;");
```

Honetaz gain, iruzkinak ere azalduko dira atal honetan,
honetarako *sql*-z gain *Ajax* ere erabiltzen da, iruzkinak
bostnaka lortzen joateko.

*Ajax* eskaera orriaren *onload* ean egingo da lehenengo aldiz
lehen 5 iruzkinak lortzeko eta ondoren erabiltzailearen eskuetan
geldituko da.

Iruzkinak lortzeko *sql* kontsulta hurrengoa da :
```
$comments = $sql->query("SELECT review, author FROM proreviews WHERE id=$id LIMIT $p,5;");
```

# Erabili diren teknologia ez derrigorrezkoak

## Ajax
Ajax teknologia, webguneari arintasuna emateko erabili da. Bi gunetan erabili dugu
 gehienbat, index.php-n eta pelikula.php-n.

### index.php
Kasu honetan, js bidez sortutako "pelikulak_atzitu" funtzioa erabili da.

```
function pelikulak_atzitu (id, val) {...}
```

Funtzio honek, globaltzat jarri ditugun bi aldagai erabiltzen ditu, eta "var"
sarrerako aldagaian jarri dugunaren arabera aldatuko ditu bi aldagai horiek.
Ondoren, ajax bidez eskaria egiten dio "pelikula_gehiago.php"-ri. Eskarian, bereizi egiten
 da ea bilaketa bat den edo ez.

Php-ak beraz, eman zaizkion aldagaien baitan sql deia egiten du eta emaitzak inprimatu.

### pelikula.php
Kasu honetan berriz, "komentarioak_atzitu" funtzioa erabili da.

```
function komentarioak_atzitu (id, pid) {...}
```

Funtzio hau lehengoaren oso antzekoa da, baina sinpleagoa. Ez bait ditugu "var"
edota "q" aldagaiak behar. Horren ordez, "pid" aldagaia jasotzen dugu, zeina
zuzenean pasatzen zaion "komentario_gehiago.php"-ri, sql bitartez pelikula
jasotzeko beharrezkoa baita.

## [Hostinger](https://www.hostinger.es)

Hostinger, webguneak hosteatzeko zerbitzua eskaintzen duen gune bat da. Horretaz
gain, datubase bat ere eskaintzen du.
Beraz, gure webgunea hemen hosteatzea erabaki dugu. Datuak lotzeko, hostingerrek
daukan git-eko zerbitzuaz baliatu gara.

## mySQL

mySQL, SQL lengoaiaz baliatzen den sistema bat da, datubaseen maneiurako.

Pelikulak xml batean gordetzea pentsaezina iruditu zitzaigun. Beraz, beste datubase
finago bat erabili nahian, eta Hostinger-ek jada zekarren zerbitzu bat zenez,
hauxe aukeratu dugu pelikulen datubase bezala.

- Konexioa egiteko php-ren sqli_connect(...) funtzioa erabili dugu, "sql.inc"-en
jadanik gordeta geneuzkan datuak erabiliz.

```
require_once("sql.inc");
```
```
$sql = mysqli_connect($hostname,$username,$password,$username);
```


Gero eskaerak sql lengoaian egin dira honela:
```
$result = $sql->query("SELECT * FROM someplace WHERE condition ORDER BY someorder LIMIT a,b;");
```

Lerroen hatzipena:
```
if($result->num_rows > 0) {
	while($row = $result->fetch_assoc()) {...}
}
```
Eta lerro barneko datuena:
```
$row["validname"]
```

# Makina birtualari buruz

Hostinger erabili dugunez, makina birtuala ez dugu erabili behar izan.

# Akats ezagunak

- Ikusteke dauden pelikula guztiak pasa ondoren, ezta berriro hasierara pasatzen.

- Azkarregi egiten badira gauzak, datubaseak batzuetan failatzen du.

# Inplementatu gabe geratu diren hobekuntzak

- Datubaseak failatzen duenean errore mezuak atera ordez, erabiltzailerari
mezu dotore bat azaltzea
