<?php
$BL_FILE='data/erabiltzaileak.xml';		// Iruzkinak gordeko diren fitxategia.

// Balioztatu e-posta helbide baten formatua.
function formatua($helbidea)
{
	$at=strpos($helbidea,'@');
	if($at === false)	// Ez da '@' karakterea aurkitu.
		return false;
	if($at == 0)			// '@' karakterea lehenengoa da.
		return false;
	if($at != strrpos($helbidea,'@'))	// '@' karakterea behin baino gehiagotan aurkitu da.
		return false;
	$dot = strrpos($helbidea,'.');
	if($dot === false) 	// Ez dago punturik.
		return false;
	if($dot < $at)			// Ez dago punturik '@' karakterearen ondoren.
		return false;
	if($dot+2 > strlen($helbidea) - 1)	// Azkeneko puntuaren atzetik ez daude gutxienez 2 karaktere.
		return false;
	return true;
}

// Balioztatu iruzkin berri bat sortzeko formularioko datuak.
// Gogoratu nahiz eta balioztapen hau bezeroan egin den, zerbitzarian berriro egitea ezinbestekoa dela.
// Errorerik egonez gero errore mezu bat itzultzen du, bestela string hutsa.
function balidatu_berria($izena, $pasahitza, $eposta)
{
	$mezua = '';
	if($izena == '')	// Izena eremua ez da bete.
		$mezua = $mezua.'<li>Izena eremua ez da bete.</li>';
	if($eposta != '' && !formatua($eposta))	// E-posta eremua bete da, baina bere formatua ez da zuzena.
		$mezua = $mezua.'<li>Posta helbidearen formatua ez da zuzena.</li>';
	if($pasahitza == '')	// Iruzkina eremua ez da bete.
		$mezua = $mezua.'<li>Pasahitza eremua ez da bete.</li>';
	return $mezua;
}

// Iruzkina datu basean gorde. Errorerik ezean true itzultzen du eta false bestela.
function gorde_erabiltzailea($izena, $pasahitza, $eposta)
{
	global $BL_FILE;	// Funtzio baten barrutik aldagai global erabiltzeko 'global' erabili behar da.

	if(!file_exists($BL_FILE))	// Iruzkinak gordetzeko XML fitxategia ez bada existitzen, sortu iruzkinik gabeko XML fitxategia.
		$bl=new SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><!DOCTYPE erabiltzaileak SYSTEM "erabiltzaileak.dtd"><erabiltzaileak azkenid="0"></erabiltzaileak>');
	else	// Bestela, kargatu XML fitxategia.
		$bl=simplexml_load_file($BL_FILE);
	if(!$bl)
		return false;

	foreach($bl->erabiltzaile as $erabiltzaile){
			if($erabiltzaile->izena == $izena||$erabiltzaile->eposta==$eposta){
				return false;
			}
		}

	$id = $bl['azkenid'] + 1;	// Kalkulatu erabiltzaile berriaren identifikadorea.
	$berria=$bl->addChild('erabiltzaile');	// Sortu 'erabiltzaile' etiketa.
	$berria['id']=$id;
	$salt = uniqid(mt_rand(), true);//sortu salt-a
	$berria->addChild('izena',$izena);
	$berria->addChild('salt',$salt);
	$berria->addChild('pasahitza',sha1($salt.$pasahitza));//Hasheatu pasahitza
	$berria->addChild('eposta',$eposta);
	$bl['azkenid']=$id;	// Eguneratu erroko 'azkenid' atributua.
	return $bl->asXML($BL_FILE);	// Gorde aldaketak fitxategian.
}

function login($izena , $pasahitza, $eposta) {

	global $BL_FILE;	// Funtzio baten barrutik aldagai global erabiltzeko 'global' erabili behar da.

	if(!file_exists($BL_FILE))	// Iruzkinak gordetzeko XML fitxategia ez bada existitzen, sortu iruzkinik gabeko XML fitxategia.
		$bl=new SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><!DOCTYPE erabiltzaileak SYSTEM "erabiltzaileak.dtd"><erabiltzaileak azkenid="0"></erabiltzaileak>');
	else	// Bestela, kargatu XML fitxategia.
		$bl=simplexml_load_file($BL_FILE);
	if(!$bl)
		return false;

	$aurkitua = false;
	foreach($bl->erabiltzaile as $erabiltzaile){
		if($erabiltzaile->izena == $izena||$erabiltzaile->eposta==$eposta){
			$id->erabiltziale['id'];
			$aurkitua=true;
			$dbPasahitza = $erabiltzaile->pasahitza;
			$salt = $erabiltzaile->salt;
			break;
		}
	}
	//Ez da izen edo posta horrekin erabiltzialerik aurkitu
	if(!$aurkitua)
		return false;

  if (sha1($salt.$pasahitza)==$dbPasahitza) {
  	// Pasahitza zuzena da.
  	$_SESSION['izena'] = $izena;
		$_SESSION['id'] = $user_id;
		return true;
	}else {
		return false;
	}
}


?>
