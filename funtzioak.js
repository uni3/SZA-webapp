function balidatuSignUp (f) {
	// Formularioko balioak irakurri
	var izena = f.izena.value;
	var pass = f.pasahitza.value;
	var eposta = f.eposta.value;

	// Ziurtatu beteta egon behar diren eremuak beteta daudela.
	var errorea = '';
	if (izena === '') {
		errorea += '\t· Izena eremua bete behar duzu.\n';
	}

	if (pass === '') {
		errorea += '\t· Pasahitzaren eremua bete behar duzu.\n';
	}

	// Ziurtatu eposta idatzia dagoela eta  helbidearen formatua zuzena dela.
	if (eposta === '' || !helbide_formatua(eposta)) {
		errorea += '\t· Eposta helbidearen formatua ez da zuzena.\n';
	}

	// Errorerik badago, mezua erakutsi.
	if (errorea !== '') {
		alert('Formularioa ez duzu ondo bete:\n' + errorea);
		return false;
	} else {
		return true;
	}
}

function balidatuLogin(f){

	// Formularioko balioak irakurri
	var izena = f.izena.value;
	var pass = f.pasahitza.value;

	// Ziurtatu beteta egon behar diren eremuak beteta daudela.
	var errorea = '';
	if (izena === '') {
		errorea += '\t· Izena eremua bete behar duzu.\n';
	}

	if (pass === '') {
		errorea += '\t· Pasahitzaren eremua bete behar duzu.\n';
	}

	// Errorerik badago, mezua erakutsi.
	if (errorea !== '') {
		alert('Formularioa ez duzu ondo bete:\n' + errorea);
		return false;
	} else {
		return true;
	}

}

function iruzkinaBalidatu (f) {
	var iruzkina = f.iruzkina.value;
	// Iruzkina hutsa ez dela egiaztatu
	if (iruzkina === '') {
		alert('Iruzkina ezin da hutsa izan');
		return false;
	}else{
		return true;
	}

}

function inData (id) {
	document.getElementById(id).innerHTML = '';
}

// E-posta helbidearen formatua zuzena bada true itzultzen du eta false bestela.
function helbide_formatua (helbidea) {
	// Ziurtatu '@' karakterea behin, eta behin bakarrik, agertzen dela.
	if (helbidea.split('@').length !== 2) {
		return false;
	}
	// Ziurtatu '@' karakterea ez dela lehena.
	if (helbidea.indexOf('@') === 0) {
		return false;
	}
	// Ziurtatu '@' karakterearen ondoren '.' karaktereren bat badagoela.
	if (helbidea.lastIndexOf('.') < helbidea.lastIndexOf('@')) {
		return false;
	}
	// Ziurtatu azkeneko puntuaren atzetik gutxienez beste 2 karaktere daudela.
	if (helbidea.lastIndexOf('.') + 2 > helbidea.length - 1) {
		return false;
	}
	return true;
}

function pelikulak_atzitu (id, val) {
	// Sortu XMLHttpRequest objektu bat (nabigatzailearen arabera modu ezberdina).
	if (val === 'next') {
		p = p + q;
	} else {
		q = parseInt(document.getElementById('fquant').value, 10);
	}
	if (document.getElementById(id).innerHTML === 'zero results') {
		p = 0;
	}

	var xhr;
	if (XMLHttpRequest) {
		xhr = new XMLHttpRequest();
	} else {
		xhr = new ActiveXObject('Microsoft.XMLHTTP');
	}

	// Adierazi erabiliko den metodoa (GET), URLa (parametro eta guzti) eta eskaera asinkronoa izango den ala ez (true).
	if (document.getElementById('kw').value) {
		p = 0;
		xhr.open('GET', 'pelikula_gehiago.php?q=' + q + '&p=' + p + '&kw=' + document.getElementById('kw').value, true);
	} else {
		xhr.open('GET', 'pelikula_gehiago.php?q=' + q + '&p=' + p, true);
	}
	// Eskaeraren egoera aldatutakoan egin beharrekoa.
	xhr.onreadystatechange = function () {
		// Egoera egokia bada (4 eta 200) dagokion elementuan idatzi zerbitzaritik jasotako testua.
		if (xhr.readyState === 4 && xhr.status === 200) {
			document.getElementById(id).innerHTML = xhr.responseText;
		}
	};
	xhr.send(''); // Bidali AJAX eskaera.
}

var q = 30;
var p = 0;

function komentarioak_atzitu (id, pid) {
	// Sortu XMLHttpRequest objektu bat (nabigatzailearen arabera modu ezberdina).
	pk = pk + 5;

	var xhr;
	if (XMLHttpRequest) {
		xhr = new XMLHttpRequest();
	} else {
		xhr = new ActiveXObject('Microsoft.XMLHTTP');
	}

	// Adierazi erabiliko den metodoa (GET), URLa (parametro eta guzti) eta eskaera asinkronoa izango den ala ez (true).

	xhr.open('GET', 'komentario_gehiago.php?p=' + pk + '&id=' + pid, true);

	// Eskaeraren egoera aldatutakoan egin beharrekoa.
	xhr.onreadystatechange = function () {
		// Egoera egokia bada (4 eta 200) dagokion elementuan idatzi zerbitzaritik jasotako testua.
		if (xhr.readyState === 4 && xhr.status === 200) {
			document.getElementById(id).innerHTML = xhr.responseText;
		}
	};
	xhr.send(''); // Bidali AJAX eskaera.
}

var pk = -5;
