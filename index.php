<?php
	session_start();
	require_once("sql.inc");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>FILM ReVIEWS AWESOME.</title>
		<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
		<link rel="stylesheet" href="index.css" type="text/css" />
		<script type="text/javascript" src="funtzioak.js"></script>
	</head>
	<body onload="javascript:pelikulak_atzitu('pelist','');">
		<?php
			if (!empty($_SESSION["izena"])) {
				echo ('<a href="itxisesioa.php">Itxi sesioa</a>');
				echo ('<p>Aupa ' .$_SESSION['izena'].' </p>');
				$_SESSION["nombre_usuario"] . " (" . $_SESSION["nombre_cliente"] . ")";
			}else{
				?>
				<p><a href="login.html" class="gehiago" >Log in</a> edo <a href="signup.html" class="gehiago">Sign up</a></p>
				<?php
			}

		 ?>
		<h1>Pelikulak</h1>
		<?php

			//connection to the database
			$sql = mysqli_connect($hostname,$username,$password,$username);

			// Check connection
			if (mysqli_connect_errno())
			{
				echo "Failed to connect to MySQL: " . mysqli_connect_error();
			}
			?>
			<div id="aukerak">
				<p>

					<select id="fquant">
						<option value="30">30</option>
						<option value="60">60</option>
						<option value="90">90</option>
						<option value="120">120</option>
					</select>

					<a href="javascript:pelikulak_atzitu('pelist','');" class="gehiago">[Aldatu tamaina]</a>

					<input id="kw" type="text" /> <a href="javascript:pelikulak_atzitu('pelist','');" class="gehiago">[Bilatu]</a> <br/>
				</p>
				<p><a href="javascript:pelikulak_atzitu('pelist','next');" class="gehiago">[Gehiago]</a></p>
			</div>
			<div id="pelist">
			</div>
			<p><a href="javascript:pelikulak_atzitu('pelist','next');" class="gehiago">[Gehiago]</a></p>
	</body>
</html>
