<?php
	session_start();
	require_once('erabiltzaileak.inc');
	require_once("sql.inc");

	// Jaso formularioko balioak eta testuei hasierako eta amaierako hutsuneak kendu (trim).
	$iruzkina=$_POST['iruzkina'];
	$izena = $_SESSION['izena'];
	$id = $_GET['id'];

	if($iruzkina===""){
		$errorea="Iruzkin hutsa utzi duzu";
	}
	//Balidatu formularioko datuak.	//connection to the database
	$sql = mysqli_connect($hostname,$username,$password,$username);

	// Check connection
	if (mysqli_connect_errno())
		{
			$errorea= "Failed to connect to MySQL: " . mysqli_connect_error();
		}
	if($errorea == ''){
		$insert = "INSERT INTO proreviews (id,review,rating,author,media) VALUES ('$id','$iruzkina','0','$izena','0')";
		if ($sql->query($insert)===FALSE) {
     			$errorea="Ezin izan da iruzkina gorde.";
		}
	}
	$sql->close();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
	<?php
		if($errorea=='')
			echo '<title>Zure iruzkina gorde da</title>';
		else
			echo '<title>Errorea iruzkina gordetzean</title>';
	?>
		<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
		<link rel="stylesheet" href="iruzkinberria.css" type="text/css" />
	</head>
	<body>
	<?php
		if($errorea != '')
		{
			echo('<h1>Errore bat gertatu da iruzkina gordetzean.</h1>');
			echo("<ul>$errorea</ul>");
		}
		else
		{
			echo('<h1>Zure iruzkina gorde da.</h1>');
		}
	?>
		<p><a href="index.php">Itzuli menu nagusira</a>.</p>
	</body>
</html>
