<?php
  session_start();
  //sesioko superglobalak ezabatu
  unset($_SESSION["izena"]);
  unset($_SESSION["id"]);
  session_destroy();
  //Hasierako orrira itzuli.
  header("Location: index.php");
  exit;
?>
