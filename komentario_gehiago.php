<?php
	require_once("sql.inc");

	// Beharrezkoa den parametroa ('id') bidali den eta iruzkinen XML fitxategia existitzen den ziurtatu.
	if( isset($_GET['id']) && isset($_GET['p'])) {
		$id=$_GET['id'];
		$p=$_GET['p'];
		if (isset($_GET['kw'])) {
			$kw=str_replace(' ', '%', $_GET['kw']);
		} else {
			$kw='';
		}
		//Connection to the database
		$sql = mysqli_connect($hostname,$username,$password,$username);

		// Check connection
		if (mysqli_connect_errno()) {
			echo "Failed to connect to MySQL: " . mysqli_connect_error();
		}



		$comments = $sql->query("SELECT review, author FROM proreviews WHERE id=$id LIMIT $p,5;");

		if($comments->num_rows > 0) {
			while($row = $comments->fetch_assoc()) {
				echo('<div class="iruzkina">');
				echo('<div class="ir_goiburua">');
				echo('<span class="egilea">'.$row["author"].'</span>');
				echo('</div>');
				echo('<div class="ir_gorputza" >');
				echo($row["review"]);
				echo('</div>');
				echo('</div>');
				echo("<br/>\n");
			}
		}else{
			if($p == 0) {
				echo('<p>Oraindik ez dago iruzkinik film honi buruz izan zaitez lehenegoa zerbait idazten!</p>');
			} else {
				echo('Iruzkin guztiak ikusi dituzu');
			}
		}

		$sql->close();
	}
?>
