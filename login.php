<?php
	require_once('erabiltzaileak.inc');
	session_start();

	// Jaso formularioko balioak eta testuei hasierako eta amaierako hutsuneak kendu (trim).
	$izena=trim($_POST['izena']);
	$pasahitza=trim($_POST['pasahitza']);
	$eposta=trim($_POST['eposta']);

	//Balidatu formularioko datuak.
	$errorea = balidatu_berria($izena, $pasahitza, $eposta);
	if($errorea == ''){
		if(!login($izena, $pasahitza, $eposta))	// Gorde erabiltzailea datu basean (XML fitxategia).
			$errorea = '<li>Izen edo pasahitza okerra.</li>';
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
	<?php
		if($errorea=='')
			echo '<title>Sisteman sartu zara</title>';
		else
			echo '<title>Errorea sisteaman sartzean</title>';
	?>
		<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
		<link rel="stylesheet" href="login.css" type="text/css" />
	</head>
	<body>
	<?php
		if($errorea != '')
		{
			echo('<h1>Izen edo pasahitza okerra.</h1>');
			echo("<ul>$errorea</ul>");
		}
		else
		{
			echo('<h1>Ongi etorri '.$_SESSION['izena'].'</h1>');
		}
	?>
		<p><a href="index.php">Itzuli menu nagusira</a>.</p>
	</body>
</html>
