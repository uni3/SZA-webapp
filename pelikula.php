<?php
	session_start();
	require_once("sql.inc");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>FILM ReVIEWS AWESOME.</title>
		<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
		<link rel="stylesheet" href="pelikula.css" type="text/css" />
		<script type="text/javascript" src="funtzioak.js"></script>
	</head>
	<?php
	if(isset($_GET['id'])){
		$pelikulaid = $_GET['id'];
		print("<body onload=\"javascript:komentarioak_atzitu('klist',$pelikulaid);\">");
				if (!empty($_SESSION["izena"])) {
					echo ('<p><a href="itxisesioa.php">Itxi sesioa</a></p>');
					echo ('<p>Aupa ' .$_SESSION['izena'].' </p>');
				}else{
					echo ('<p><a href="login.html">Log in</a></p>');
				}

			 ?>
			<h1>Pelikularen informazioa:</h1>
			<?php

				//connection to the database
				$sql = mysqli_connect($hostname,$username,$password,$username);

				// Check connection
				if (mysqli_connect_errno())
				{
					echo "Failed to connect to MySQL: " . mysqli_connect_error();
				}else{

					$result = $sql->query("SELECT * FROM films WHERE id=$pelikulaid;");
					//Beti egongo da pelikula aukeratua izan den moduagatik
					$row = $result->fetch_assoc();
					echo('<div id="Informazioa">'."\n");
					echo( '<img id="Irudia" src="'.$row["image"].'" alt="'.$row["name"].'"/>'."\n");
					echo('<div id="rating">');

					//Izarren irudia ager dadin
					switch ($row["rating"][0]) {
						case '0':
							echo( '<img src="http://www.filmaffinity.com/imgs/ratings/0.png" alt="'.$row["rating"].'"/>');
							break;
						case '1':
							echo( '<img src="http://www.filmaffinity.com/imgs/ratings/1.png" alt="'.$row["rating"].'"/>');
							break;
						case '2':
							echo( '<img src="http://www.filmaffinity.com/imgs/ratings/2.png" alt="'.$row["rating"].'"/>');
							break;
						case '3':
							echo( '<img src="http://www.filmaffinity.com/imgs/ratings/3.png" alt="'.$row["rating"].'"/>');
							break;
						case '4':
							echo( '<img src="http://www.filmaffinity.com/imgs/ratings/4.png" alt="'.$row["rating"].'"/>');
							break;
						case '5':
							echo( '<img src="http://www.filmaffinity.com/imgs/ratings/5.png" alt="'.$row["rating"].'"/>');
							break;
						case '6':
							echo( '<img src="http://www.filmaffinity.com/imgs/ratings/6.png" alt="'.$row["rating"].'"/>');
							break;
						case '7':
							echo( '<img src="http://www.filmaffinity.com/imgs/ratings/7.png" alt="'.$row["rating"].'"/>');
							break;
						case '8':
							echo( '<img src="http://www.filmaffinity.com/imgs/ratings/8.png" alt="'.$row["rating"].'"/>');
							break;
						case '9':
							echo( '<img src="http://www.filmaffinity.com/imgs/ratings/9.png" alt="'.$row["rating"].'"/>');
							break;
						case '10':
							echo( '<img src="http://www.filmaffinity.com/imgs/ratings/10.png" alt="'.$row["rating"].'"/>');
							break;
					}
					echo('<p>'.$row["rating"].'</p>');
					echo('</div>'."\n");

					//Pelikularen datuak agertuko dira bertan.
					echo('<dl>'."\n");
						echo('<dt>Izenburua');
						echo('</dt>');
						echo('<dd>'.$row["name"]);
						echo('</dd>'."\n");
						echo('<dt>Urtea');
						echo('</dt>');
						echo('<dd>'.$row["year"]);
						echo('</dd>'."\n");
						echo('<dt>Iraupena');
						echo('</dt>');
						echo('<dd>'.$row["duration"]);
						echo('</dd>'."\n");
						echo('<dt>Herrialdea');
						echo('</dt>');
						echo('<dd>'.$row["country"]);
						echo('</dd>'."\n");
						echo('<dt>Zuzendaria');
						echo('</dt>');
						echo('<dd>'.$row["director_string"]);
						echo('</dd>'."\n");
						echo('<dt>Musika');
						echo('</dt>');
						echo('<dd>'.$row["music"]);
						echo('</dd>'."\n");
						echo('<dt>Aktore-zerrenda');
						echo('</dt>');
						echo('<dd>'.$row["cast_string"]);
						echo('</dd>'."\n");
						echo('<dt>Generoa');
						echo('</dt>');
						echo('<dd>'.$row["genres_string"]);
						echo('</dd>'."\n");
						echo('<dt>Sinopsis');
						echo('</dt>');
						echo('<dd>'.$row["synopsis"]);
						echo('</dd>'."\n");
					echo('</dl>'."\n");
					echo("</div>\n");

					//Komentarioak ajax bidez lortu.
					echo("<p><a href=\"javascript:komentarioak_atzitu('klist',$pelikulaid);\" class=\"gehiago\">[Gehiago]</a></p>");
					?>
					<div id="klist">
					</div>
					<?php
					//Iruzkinak idazteko kutxa.
					echo("<p><a href=\"javascript:komentarioak_atzitu('klist',$pelikulaid);\" class=\"gehiago\">[Gehiago]</a><br/></p>");
					if (!empty($_SESSION["izena"])) {
					echo('<form action="iruzkinberria.php?id='.$pelikulaid.'" method="post">
						<p>Iruzkina(*):<textarea name="iruzkina" id="iruzkina" rows="15" cols="80" onclick="javascript:inData("iruzkina")">Idatzi zure iruzkina!</textarea></p>
						<p><input type="submit" onclick="return iruzkinaBalidatu(this.form)" /></p>
						</form>');
					}else{
						echo ('<p><a href="login.html">Iruzkinak idatzi ahal izateko sisteman sartu beharra duzu.</a></p>'."\n");
					}

					//Datu basearekin konexioa itxi.
					$sql->close();

				}
			}
				?>

		</body>


</html>
