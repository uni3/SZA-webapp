<?php
	require_once("sql.inc");

	// Beharrezkoa den parametroa ('id') bidali den eta iruzkinen XML fitxategia existitzen den ziurtatu.
	if( isset($_GET['q']) && isset($_GET['p'])) {
		$q=$_GET['q'];
		$p=$_GET['p'];
		if (isset($_GET['kw'])) {
			$kw=str_replace(' ', '%', $_GET['kw']);
		} else {
			$kw='';
		}
		//Connection to the database
		$sql = mysqli_connect($hostname,$username,$password,$username);

		// Check connection
		if (mysqli_connect_errno()) {
			echo "Failed to connect to MySQL: " . mysqli_connect_error();
		}

		$result = $sql->query("SELECT id, name, image FROM films WHERE name LIKE '%$kw%' ORDER BY name LIMIT $p,$q;");
		$to = $p + $q;
		echo("Results from $p to $to");

		if(isset($_GET['kw'])) {
			echo (" with '$kw' keyword.");
		} else {
			echo(".");
		}

		echo(" In total, $result->num_rows films were found. </br>\n");
		if($result->num_rows > 0) {
			while($row = $result->fetch_assoc()) {

				echo('<a href=pelikula.php?id='.$row["id"].' id="'.$row["id"].'">');
				echo("\t - Name: " . $row["name"]);
				echo( '<img src="'.$row["image"].'" alt="'.$row["name"].'" height="130" width="130"/>');
				echo("</a><br/>\n");
			}

		} else {
			echo("zero results");

		}

		$sql->close();
	}
?>
