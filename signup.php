<?php
	require_once('erabiltzaileak.inc');

	// Jaso formularioko balioak eta testuei hasierako eta amaierako hutsuneak kendu (trim).
	$izena=trim($_POST['izena']);
	$pasahitza=trim($_POST['pasahitza']);
	$eposta=trim($_POST['eposta']);

	//Balidatu formularioko datuak.
	$errorea = balidatu_berria($izena, $pasahitza, $eposta);
	if($errorea == '')
		if(!gorde_erabiltzailea($izena, $pasahitza, $eposta))	// Gorde erabiltzailea datu basean (XML fitxategia).
			$errorea = '<li>Izen edo posta helbide horrekin jada erabiltzaile bat badago erregistraturik.</li>';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
	<?php
		if($errorea=='')
			echo '<title>Eskerrik asko sisteman erregistratzeagatik</title>';
		else
			echo '<title>Errorea sisteaman erregistratzean</title>';
	?>
		<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
		<link rel="stylesheet" href="signup.css" type="text/css" />
	</head>
	<body>
	<?php
		if($errorea != '')
		{
			echo('<h1>Errore bat gertatu da sisteman erregistratzean.</h1>');
			echo("<ul>$errorea</ul>");
		}
		else
		{
			echo('<h1>Eskerrik asko sisteman erregistratzeagatik.</h1>');
		}
	?>
		<p><a href="index.php">Itzuli menu nagusira</a>.</p>
	</body>
</html>
